import React, { Component } from 'react';
import CartItem from './CartItem';


export default class Cart extends Component {

    constructor(props) {
        super(props)

        this.state = {
            cartMsg: "",
        }
    }

    render() {

        if (this.calculateCartTotal() === 0) {
            return <p>Cart is Empty.</p>
        } else
            return (
                <div>
                    <button onClick={() => this.props.handleCart('reset')}>Reset</button>
                    <table className="cart-table">
                        <tr>
                            <th>DELETE</th>
                            <th>BOOK</th>
                            <th>QUANTITY</th>
                            <th>TOTAL</th>
                        </tr>
                        {
                            this.props.cart.map(cartItem => <CartItem key={cartItem.id} cartItem={cartItem} handleCart={this.props.handleCart} />)
                        }
                        <tr>
                            <td colSpan="3">Grand Total</td>
                            <td>{this.calculateCartTotal()}</td>
                        </tr>
                    </table>
                    <button onClick={this.handleOrder}>Order</button>
                    {this.state.cartMsg}
                </div>
            )
    }

    handleOrder = () => {
        if (this.props.user.loggedIn === true) {
            const order = {
                username: this.props.user.username,
                cart: this.props.cart.map(item => ({ "bookid": item.id, "quantity": item.quantity })),
                timestamp: Date.now()
            }
            //axios post order then show successful message

            this.setState({
                cartMsg: `Order placed of ${this.calculateCartTotal()}.
                Thanks for Shopping with us...`
            })
            setTimeout(() => this.props.handleCart('reset'), 2000);
        } else {
            this.setState({
                cartMsg: 'Log In First To Place A Order.'
            })
        }

    }

    calculateCartTotal = () => {
        return this.props.cart.reduce((acc, curr) => acc + (curr.price * curr.quantity), 0);
    }
}
