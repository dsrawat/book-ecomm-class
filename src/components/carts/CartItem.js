import React from 'react'

export default function CartItem(props) {
    return (
        <tr className="cart-item">
            {/* <img src="" alt="" /> */}
            <td><i onClick={() => props.handleCart('delete', props.cartItem)} className="fa fa-trash-o cart-delete" aria-hidden="true"></i></td>
            <td className="cart-content">
                <p><i>Title</i> : {props.cartItem.title}<br />
                    <i>Author</i> : {props.cartItem.author}<br />
                    <i>Price</i> : {props.cartItem.price}</p>
            </td>
            <td>
                <i onClick={() => props.handleCart('increment', props.cartItem)} className="fa fa-plus-square-o cart-increment" aria-hidden="true"></i>
                <br />{props.cartItem.quantity}<br />
                <i onClick={() => props.handleCart('decrement', props.cartItem)} className="fa fa-minus-square-o cart-decrement" aria-hidden="true"></i>
            </td>
            <td>{props.cartItem.price * props.cartItem.quantity}</td>
        </tr>
    )
}
