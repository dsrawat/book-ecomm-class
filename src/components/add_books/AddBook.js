import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

export default class AddBook extends Component {

    constructor(props) {
        super(props)

        this.state = {
            bookDataPostMsg: "",
            redirectFlag: false
        }
    }

    render() {

        if (!this.state.bookDataPostMsg) {
            return (
                <div>
                    <form onSubmit={this.handleBookDataSubmit}>
                        Upload Image<input type="file" name="pic" accept="image/*" />
                        <input type="text" name="title" placeholder="title" />
                        <input type="text" name="author" placeholder="author" />
                        <input type="text" name="category" placeholder="category" />
                        <input type="text" name="price" placeholder="price" />
                        <input type="text" name="description" placeholder="description" />
                        <input type="text" name="inventory" placeholder="inventory" />
                        <input type="submit" />
                    </form>
                </div>
            )
        } else {
            return (
                <div>
                    <div className="alert-msg">{this.state.bookDataPostMsg}</div>
                    {!this.state.redirectFlag ? <Redirect to="/" /> : null}
                </div>
            )
        }

    }

    handleBookDataSubmit = event => {
        event.preventDefault();
        console.log("Data Added");
        //axios.post(book data) 
        // after posting
        this.setState({
            bookDataPostMsg: "Book Successfully added to the database. Redirecting to home page..."
        })
        setTimeout(() => {
            this.setState({
                redirectFlag: true
            })
        }, 1000)
    }
}