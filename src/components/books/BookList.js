import Book from './Book';
import React, { Component } from 'react';
import AddBook from '../add_books/AddBook';
import { __books } from '../../__datas/__books';
import __getFilteredBooks from '../../__utilities/__getFilteredBooks';
import { Link } from 'react-router-dom';

export default class BookList extends Component {

    constructor(props) {
        super(props)

        this.state = {
            addBookFlag: false,
            bookWarningMsg: "",
            books: []
        }
    }

    componentDidMount = () => {
        //const books = axios.get(bookList)
        this.changeBooks();

    }

    componentDidUpdate = (prevProps) => {
        const { searchedFilter, searchedContent } = this.props.searchBar;
        if (prevProps.searchBar.searchedFilter !== searchedFilter || prevProps.searchBar.searchedContent !== searchedContent) {
            this.changeBooks();
        }
    }

    render() {
        return (
            <div>
                {
                    this.props.user.role ?
                        <p><Link to="addbook" >
                            Add a new Book
                        </Link></p> : null
                }
                {
                    this.state.books.map(book => <Book key={book.title} handleCart={this.props.handleCart} book={book} />)
                }
                <hr />
            </div>
        )

    }

    changeBooks() {
        const { searchedFilter, searchedContent } = this.props.searchBar;
        this.setState({
            books: __getFilteredBooks(searchedFilter, searchedContent, __books)
        });
    }
}
