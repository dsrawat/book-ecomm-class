import React from 'react';
import { Link } from 'react-router-dom';
import a_doll_house from '../../images/a-Dolls-house.jpg';

export default function Book(props) {
    return (
        <div className="book">
            {/* <img src={a_doll_house} alt="a doll house" /> */}
            <Link to={`/book/${props.book.title}`}>
                <p>Title : {props.book.title}</p>
                <p>Author : {props.book.author}</p>
                <p>Country : {props.book.country}</p>
                <p>Price : {props.book.price}</p>
            </Link>
            <button onClick={() => props.handleCart('add', props.book)}>Add To Cart</button>
        </div>
    )
}