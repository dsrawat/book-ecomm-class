import React, { Component } from 'react';
import { __books } from '../../__datas/__books';
import { Redirect } from 'react-router-dom';
import a_doll_house from '../../images/a-Dolls-house.jpg';



export default class BookDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {
            redirectFlag: false
        }
    }

    render() {

        const bookTitle = this.props.match.params.title;
        const [book] = __books.filter(book => (book.title === bookTitle));

        return (
            <div className="book">
                {/* <img src={a_doll_house} alt="a doll house" /> */}
                {this.state.redirectFlag ? <Redirect exact to="/cart" /> : null}
                {Object.keys(book).map(key => <p>{key} : {book[key]}</p>)}
                <button onClick={this.handleButtonClick}>Add To Cart</button>
            </div>
        )
    }

    handleButtonClick = () => {

        const bookTitle = this.props.match.params.title;
        const [book] = __books.filter(book => (book.title === bookTitle));

        this.props.handleCart('add', book);
        this.setState({
            redirectFlag: true
        });
    }
}

