import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

export default class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            username: "",
            password: "",
            logMsg: "",
            loggedIn: false
        }
    }

    render() {
        return (
            <div className="login">
                <p className="form-title">Login</p>
                <form onSubmit={this.handleSubmit}>
                    <label for="username">Username</label>
                    <input type="text" name="username"
                        onChange={this.handleInput}
                        value={this.state.username} required />

                    <label for="password">Password</label>
                    <input type="password" name="password"
                        onChange={this.handleInput}
                        value={this.state.password} required />
                    <button type="submit">Login</button>
                </form>
                <Link to="/register">
                    New User? Register Here...
            </Link>

                <p style={{ color: 'gray' }}>{this.state.logMsg}</p>
                {this.state.loggedIn ? <Redirect to="/" /> : null}
            </div>)
    }

    handleInput = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit = event => {

        event.preventDefault();
        //     postReq(__server_apis.login, {
        //         "username": this.state.username,
        //         "password": this.state.password
        //     })
        //         .then(res => {

        //             this.props.handleUser(true, this.state.username, "admin", res.data.accessToken)
        //         }).then(() => {
        //             this.setState({
        //                 logMsg: 'Succesfully Logged In. Redirecting to Home Page ...'
        //             });
        //             setTimeout(() => {
        //                 this.setState({
        //                     loggedIn: true
        //                 })
        //             }, 1000);
        //         }).catch(err => {
        //             setLogMsg('Something Went Wrong...');
        //         });

        this.props.handleUser(true, this.state.username, "admin", 'gdg5yh65gvdrt5u546gr6y54h');
        this.setState({
            logMsg: 'Succesfully Logged In. Redirecting to Home Page ...'
        });
        setTimeout(() => {
            this.setState({
                loggedIn: true
            })
        }, 1000);
    }
}
