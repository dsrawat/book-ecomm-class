import React from 'react';
import { NavLink } from "react-router-dom";

export default function NavAccountDetails(props) {
    return (
        <div className="nav-account-details">
            <NavLink onClick={() => props.clearAllSearchState(true)} activeClassName="active" to="/accountdetails">
                <i className="fa fa-user" aria-hidden="true"> Account Details</i>
            </NavLink>
        </div>
    )
}
