import React from 'react'

export default function NavSearchBar(props) {
    return (
        <div className="nav-search-bar">
            <form onSubmit={submitSearchHandler}>
                <select name="localSearchedFilter"
                    onChange={localSearchHandler}
                    value={props.localSearchState.localSearchedFilter}>

                    <option>all</option>
                    <option>title</option>
                    <option>author</option>
                    <option>category</option>
                    <option>popular</option>
                    <option>price</option>

                </select>

                <input type="search"
                    name="localSearchedContent"
                    onChange={localSearchHandler}
                    value={props.localSearchState.localSearchedContent} />

                <button type="submit">
                    <i className="fa fa-search" aria-hidden="true"></i>
                </button>
            </form>
        </div>
    )

    function localSearchHandler(event) {
        const localSearchedFilter = event.target.name === 'localSearchedFilter' ? event.target.value : 'all';
        const localSearchedContent = event.target.name === 'localSearchedContent' ? event.target.value : '';
        props.handleLocalSearchState(false, localSearchedFilter, localSearchedContent);
    }

    function submitSearchHandler(event) {
        event.preventDefault();
        props.onSearch(false, props.localSearchState.localSearchedFilter, props.localSearchState.localSearchedContent);
    }
}


