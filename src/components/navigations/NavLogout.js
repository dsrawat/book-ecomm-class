import React from 'react';

export default function NavLogout(props) {
    return (
        <div className="nav-logout">
            <button onClick={handleLogout} ><i className="fa fa-sign-out" aria-hidden="true"> SignOut</i></button>
        </div>
    )

    function handleLogout() {
        props.clearAllSearchState(true);
        props.handleUser();
    }
}
