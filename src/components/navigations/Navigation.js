import React, { Component } from 'react'
import NavSiteLogo from './NavSiteLogo'
import NavSearchBar from './NavSearchBar'
import NavLogin from './NavLogin'
import NavLogout from './NavLogout'
import NavAccountDetails from './NavAccountDetails'
import NavCart from './NavCart';

const initialLocalSearchState = {
    localSearchedFilter: 'all',
    localSearchedContent: ''
}


export default class Navigation extends Component {

    constructor(props) {
        super(props);

        this.state = { ...initialLocalSearchState }
    }

    render() {
        const { searchBar, onSearch } = this.props;
        return (
            <div className="nav-bar">

                <NavSiteLogo clearAllSearchState={this.clearAllSearchState} />

                <NavSearchBar onSearch={onSearch} searchBar={searchBar}
                    localSearchState={{
                        localSearchedFilter: this.state.localSearchedFilter,
                        localSearchedContent: this.state.localSearchedContent
                    }}
                    handleLocalSearchState={this.handleLocalSearchState} />


                {
                    this.props.user.loggedIn ?
                        <NavLogout handleUser={this.props.handleUser} clearAllSearchState={this.clearAllSearchState} /> :
                        <NavLogin clearAllSearchState={this.clearAllSearchState} />
                }

                {this.props.user.loggedIn && <NavAccountDetails clearAllSearchState={this.clearAllSearchState} />}

                <NavCart getTotalCartItems={this.props.getTotalCartItems} clearAllSearchState={this.clearAllSearchState} />

            </div>
        )
    }

    handleLocalSearchState = (clear, localSearchedFilter, localSearchedContent) => {
        if (clear === true) {
            this.setState({
                ...initialLocalSearchState
            })
        } else {
            this.setState({
                localSearchedFilter,
                localSearchedContent
            })
        }
    }

    clearAllSearchState = clear => {
        if (clear === true) {
            this.handleLocalSearchState(true);
            this.props.onSearch(true);
        }
    }
}