import React from 'react';
import { NavLink } from 'react-router-dom';

export default function NavSiteLogo(props) {
    return (
        <div className="nav-site-logo">
            <NavLink onClick={() => props.clearAllSearchState(true)} exact activeClassName="active" to="/">Book Store</NavLink>
        </div>
    )
}
