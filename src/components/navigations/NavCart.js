import React from 'react';
import { NavLink } from 'react-router-dom';

export default function NavCart(props) {
    return (
        <div className="nav-cart">
            <NavLink onClick={() => props.clearAllSearchState(true)} exact activeClassName="active" to="/cart">
                <i className="fa fa-shopping-cart cart-icon" aria-hidden="true"></i>
                {props.getTotalCartItems}
            </NavLink>
        </div>
    )
}
