import React from 'react';
import { NavLink } from 'react-router-dom';

export default function NavLogin(props) {
    return (
        <div className="nav-login">
            <NavLink onClick={() => props.clearAllSearchState(true)} activeClassName="active" to="/login">
                <i className="fa fa-sign-in" aria-hidden="true"> SignIn</i>
            </NavLink>
        </div>
    )
}
