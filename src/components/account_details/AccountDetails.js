import React from 'react'
import UserInfo from './UserInfo'
import OrderHistory from './OrderHistory'

export default function AccountDetails(props) {
    return (
        <div>
            <hr />
            <UserInfo user={props.user} />
            {/* <button >Order History</button>
            <OrderHistory /> */}
            <hr />
        </div>
    )
}
