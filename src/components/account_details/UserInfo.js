import React from 'react'

export default function UserInfo(props) {
    return (
        <div>
            <hr />
            <p>Username : {props.user.username}</p>
            <p>User Type : {props.user.role}</p>
        </div>
    )
}
