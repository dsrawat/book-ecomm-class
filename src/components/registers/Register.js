import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

export default class Register extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: "",
            email: "",
            contact: "",
            age: 21,
            address: "",
            password: "",
            registerMsg: "",
            isRegistered: false
        }
    }

    render() {
        return (
            <div className="register">
                <p className="form-title">Register Here</p>
                <form name="signup-form" onSubmit={this.submitHandler}>
                    <label for="name">Name</label>
                    <input id="firstname" name="name" type="text" title="Please enter your name" placeholder="name" autofocus required value={this.state.name} onChange={this.changeHandler} />

                    <label for="email">Email</label>
                    <input id="email" name="email" type="email" title="Please enter your email address" placeholder="email" required value={this.state.email} onChange={this.changeHandler} />

                    <label for="contact">Contact</label>
                    <input id="contact" name="contact" type="tel" pattern="[0-9]{8,}" title="Please enter your contact number, it must contains atleast 8 numbers" placeholder="contact" value={this.state.contact} onChange={this.changeHandler} />

                    <label for="age">Age</label>
                    <input id="age" type="number" size="6" name="age" min="8" max="99" value={this.state.age} onChange={this.changeHandler} />

                    <label for="address">Address</label>
                    <input id="address" type="text" name="address" pattern=".{6,}" title="Please enter your address, address must be atleast 6 characters long." value={this.state.address} onChange={this.changeHandler} />

                    <label for="password">Password</label>
                    <input id="password" name="password" type="password" title="Please enter a password, it must contain at least 1 lowercase and 1 uppercase character and be at least 6 characters in length" pattern=".*(?=.{6,})(?=.*[a-z])(?=.*[A-Z]).*" placeholder="password" required value={this.state.password} onChange={this.changeHandler} />

                    <input type="submit" value="Signup!" />

                </form>
                <p style={{ color: 'gray' }}>{this.state.registerMsg}</p>
                {this.state.isRegistered ? <Redirect to="/login" /> : null}
            </div>
        )
    }

    changeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHandler = event => {
        event.preventDefault();
        this.setState({
            registerMsg: 'Successfully Registered. Redirecting to login page...'
        })
        setTimeout(() => this.setState({ isRegistered: true }), 1000);
        console.log(this.state)
    }
}
