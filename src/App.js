import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Navigation from './components/navigations/Navigation';
import BookList from './components/books/BookList';
import BookDetails from './components/book_details/BookDetails';
import Login from './components/logins/Login';
import Register from './components/registers/Register';
import AccountDetails from './components/account_details/AccountDetails';
import Cart from './components/carts/Cart';
import __handleCart from './__utilities/__handleCart';
import AddBook from './components/add_books/AddBook';


const initialSearchBar = {
    searchedFilter: "all",
    searchedContent: ""
}

const initialUser = {
    loggedIn: false,
    username: "",
    role: "",
    jwt: ""
}

export default class App extends Component {

    constructor(props) {
        super(props)

        this.state = {
            searchBar: { ...initialSearchBar },
            cart: [],
            user: { ...initialUser }
        }
    }

    componentDidMount = () => {
        this.getPersistState();
        this.setPersistState();
    }

    render() {
        console.log('User')
        console.log(this.state.user)
        return (
            <Router>
                <div className="App">
                    <Navigation
                        searchBar={{
                            searchedFilter: this.state.searchBar.searchedFilter,
                            searchedContent: this.state.searchBar.searchedContent
                        }}
                        onSearch={this.handleSearch}
                        getTotalCartItems={this.getTotalCartItems()}
                        handleUser={this.handleUser}
                        user={this.state.user}
                    />

                    <main>
                        <Switch>
                            <Route exact path="/"
                                render={props => <BookList {...props}
                                    searchBar={{
                                        searchedFilter: this.state.searchBar.searchedFilter,
                                        searchedContent: this.state.searchBar.searchedContent
                                    }}
                                    handleCart={this.handleCart}
                                    user={this.state.user}
                                />}
                            />

                            <Route path="/book/:title"
                                render={props => <BookDetails {...props}
                                    handleCart={this.handleCart}
                                />}
                            />

                            <Route exact path="/login"
                                render={props => <Login {...props}
                                    handleUser={this.handleUser}
                                />}
                            />

                            <Route exact path="/register">
                                <Register />
                            </Route>

                            <Route exact path="/accountdetails"
                                render={props => <AccountDetails {...props}
                                    user={this.state.user}
                                    handleUser={this.handleUser}
                                />}
                            />

                            <Route exact path="/cart"
                                render={props => <Cart {...props}
                                    cart={this.state.cart}
                                    handleCart={this.handleCart}
                                    user={this.state.user}
                                />}
                            />

                            <Route exact path="/addbook">
                                {
                                    (this.state.user.role === 'admin') ? <AddBook /> : <Redirect to="/" />
                                }
                            </Route>

                        </Switch>
                    </main>
                </div>
            </Router>
        );
    }

    handleCart = (command = '', bookObj = {}) => {

        const cartCopy = [...this.state.cart]
        this.setState({
            cart: __handleCart(command, cartCopy, bookObj)
        })
    }

    handleSearch = (setDefault = false, searchedFilter = "all", searchedContent = "") => {
        if (setDefault === true) {
            this.setState({
                searchBar: { ...initialSearchBar }
            })
            return;
        }
        this.setState({
            searchBar: { searchedFilter, searchedContent }
        })
    }

    handleUser = (loggedIn = false, username = "", role = "", jwt = "") => {

        if (loggedIn === true) {
            this.setState({
                user: {
                    loggedIn,
                    username,
                    role,
                    jwt
                }
            })
        } else {
            this.setState({
                user: { ...initialUser }
            })
        }

    }

    setPersistState = () => {
        window.addEventListener('beforeunload', () => {
            localStorage.setItem('cart', JSON.stringify(this.state.cart));
            localStorage.setItem('user', JSON.stringify(this.state.user));
        })
    }

    getPersistState = () => {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const user = JSON.parse(localStorage.getItem('user'));

        if (cart !== null) {
            this.setState({ cart });
        }
        if (user !== null) {
            this.setState({ user });
        }
    }

    getTotalCartItems = () => {
        if (this.state.cart.length < 1) return ""
        return this.state.cart.reduce((acc, curr) => acc + curr.quantity, 0);
    }
}
