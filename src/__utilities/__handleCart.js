const __handleCart = (command = '', cart = [], bookObj = {}) => {
    switch (command) {

        case 'add':
            {
                const [filteredBook] = cart.filter(book => book.id === bookObj.id);
                if (!filteredBook) {
                    return ([...cart, {
                        id: bookObj.id,
                        title: bookObj.title,
                        author: bookObj.author,
                        price: bookObj.price,
                        quantity: 1
                    }])
                } else return cart;
            }

        case 'delete':
            {
                const CART_ITEMS = cart.filter(book => book.id !== bookObj.id);
                return [...CART_ITEMS];
            }

        case 'increment':
            {
                return cart.map(book => {
                    if (book.id === bookObj.id) book.quantity++;
                    return book;
                })
            }
        case 'decrement':
            {
                const [filteredBook] = cart.filter(book => book.id === bookObj.id);
                if (filteredBook.quantity > 1) {
                    return cart.map(book => {
                        if (book.id === bookObj.id) book.quantity--;
                        return book;
                    })
                }
                const CART_ITEMS = cart.filter(book => book.id !== bookObj.id);
                return [...CART_ITEMS];
            }

        case 'reset':
            return [];

        default:
            return cart;

    }
}

export default __handleCart;