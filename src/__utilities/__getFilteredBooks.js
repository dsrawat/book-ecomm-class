export default function __getFilteredBooks(searchedFilter = "all", searchedContent = "", books = []) {
    if (searchedContent === "") return books;
    switch (searchedFilter) {
        case 'all':
            return books.filter(book =>
                matchSting(book.title, searchedContent) || matchSting(book.author, searchedContent)
                || matchSting(book.category, searchedContent)
            );

        case 'title':
            return books.filter(book => matchSting(book.title, searchedContent));

        case 'author':
            return books.filter(book => matchSting(book.author, searchedContent));

        case 'category':
            return books.filter(book => matchSting(book.category, searchedContent));

        case 'price':
            if (!isNaN(searchedContent)) {
                return books.filter(book => book.price <= searchedContent);
            } else
                return books;

        default:
            return books;

    }
}


function matchSting(str = "", subStr = "") {
    return str.toLowerCase().includes(subStr.toLowerCase())
}